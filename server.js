const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('event_manager_db','root','welcome12#',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Location = sequelize.define('location', {
	city : Sequelize.STRING,
	address : Sequelize.STRING
}, {
	underscored : true
})

const Event = sequelize.define('event', {
	name : Sequelize.STRING,
	date : Sequelize.DATE,
	location: Sequelize.STRING
}, {
	underscored : true
})

Location.hasMany(Event)

const app = express()
app.use(bodyParser.json())

app.get('/create', (req, res) => {
	sequelize.sync({force : true})
		.then(() => res.status(201).send('recreated all tables'))
		.catch(() => res.status(500).send('hm, that was unexpected...'))	
})

app.get('/locations', (req, res) => {
	Location.findAll()
		.then((results) => {
			res.status(200).json(results)
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))			
})

app.post('/locations', (req, res) => {
	Location.create(req.body)
		.then(() => {
			res.status(201).send('created')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/locations/:id', (req, res) => {
	Location.findById(req.params.id)
		.then((result) => {
			if (result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.put('/locations/:id', (req, res) => {
	Location.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.update(req.body)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('modified')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.delete('/locations/:id', (req, res) => {
	Location.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.destroy()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('removed')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/events/:bid/events', (req, res) => {
	Event.findById(req.params.bid)
		.then((result) => {
			if (result){
				return result.getChapters()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then((results) => {
			res.status(200).json(results)
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.get('/locations/:bid/events/:cid', (req, res) => {
	Event.findById(req.params.bid)
		.then((result) => {
			if (result){
				return result.getEvents({where : {id : req.params.cid}})
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then((result) => {
			if (result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))	
})

app.post('/events/:bid/locations', (req, res) => {
	Event.findById(req.params.bid)
		.then((result) => {
			if (result){
				let event = req.body
				event.location_id = result.id
				return Event.create(event)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('created')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'));
})

app.put('/locations/:bid/events/:cid', (req, res) => {
	Event.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.update(req.body)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('modified')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.delete('/locations/:bid/events/:cid', (req, res) => {
	Event.findById(req.params.cid)
		.then((result) => {
			if (result){
				return result.destroy()
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('removed')
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

app.listen(8080)